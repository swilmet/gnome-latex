Code conventions
================

For consistency, there are some conventions to follow for the code.

For Vala and C
--------------

- No trailing spaces.
- Use blank lines to space out blocks of code (only one blank line is enough).
- Space out each case in a switch, i.e. have a blank line after each `break;`.
- As a general rule of thumb, when modifying a file use the same coding style of
  that file.

For Vala
--------

- Indentation: 4 spaces.
- Lines: 90 characters maximum (in some cases it can be a little more).
- `/* ... */` comments for delimiting code sections.
- `// ...` comments otherwise (e.g. for explaining just one line).
- Curly braces are always on a separate line.
- For one-line blocks (if, for, while, etc), no curly braces.
- Some spaces almost everywhere:
```
function (blah);              // not function(blah);
int num = 5;                  // not int num=5;
if (!foo)                     // not if(!foo)
for (int i = 0; i < max; i++) // not for(int i=0;i<max;i++)
// etc.
```
- Do not use `var` for declaring variables, unless the type is very long. The
  type of a variable is useful to understand the code, it is a form of
  self-documentation.
- Functions with a lot of parameters: when exceeding 90 characters, add a
  newline + one indentation level (the same for function declarations and
  function calls):
```
function_call (a_long_parameter1, a_long_parameter2, a_long_parameter3, a_long_parameter4,
    a_long_parameter5, a_long_parameter6);
```

For C
-----

- Follow the same coding style as libgedit-gtksourceview:
  https://gitlab.gnome.org/World/gedit/libgedit-gtksourceview/-/blob/main/HACKING
- No maximum line length (but short lines are better).
- Function declarations: follow the same style as what the gcu-lineup-parameters
  script does, see: https://gitlab.gnome.org/swilmet/gdev-c-utils
- Function calls with a lot of parameters: one parameter per line, aligned on
  the opening parenthesis:
```
function_call (a_long_parameter1,
	       a_long_parameter2,
	       a_long_parameter3);
```
In some cases, groups of parameters can be on the same line, when the parameters
are related (typically a string + value). For example with `g_object_new()` to
set properties:
```
return g_object_new (NAMESPACE_TYPE_CLASSNAME,
		     "property1", value1,
		     "property2", value2,
		     NULL);
```
