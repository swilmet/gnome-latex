Enter TeX
=========

![icon!](data/icons/org.gnome.enter_tex.svg)

Enter TeX is a TeX/[LaTeX](https://www.latex-project.org/) text editor. The
application was previously named LaTeXila and then GNOME LaTeX. Its development
started in 2009.

Notable features
----------------

- Build tools: customizable buttons to compile, convert and view a document in
  one click. [Latexmk](https://www.cantab.net/users/johncollins/latexmk/) is
  used by default, but the low-level commands such as pdflatex, dvipdf and
  bibtex can also be used
  ([screenshot 1](https://gitlab.gnome.org/swilmet/enter-tex-extra/-/raw/main/screenshots/screenshot-build-tools.png),
  [screenshot 2](https://gitlab.gnome.org/swilmet/enter-tex-extra/-/raw/main/screenshots/screenshot-build-tools-more-details.png)
  and [screenshot 3](https://gitlab.gnome.org/swilmet/enter-tex-extra/-/raw/main/screenshots/screenshot-build-tools-preferences.png)).

- Completion of LaTeX commands
  ([screenshot](https://gitlab.gnome.org/swilmet/enter-tex-extra/-/raw/main/screenshots/screenshot-completion.png)).

- Document structure: a list of chapters, sections, figures, etc. to easily
  navigate in a document
  ([screenshot](https://gitlab.gnome.org/swilmet/enter-tex-extra/-/raw/main/screenshots/screenshot-structure.png)).

- Symbol tables: Greek letters, arrows, etc
  ([screenshot](https://gitlab.gnome.org/swilmet/enter-tex-extra/-/raw/main/screenshots/screenshot-symbols.png)).

- Templates for creating a new document: there are a few default templates, and
  you can create personal templates
  ([screenshot](https://gitlab.gnome.org/swilmet/enter-tex-extra/-/raw/main/screenshots/screenshot-templates.png)).

- Easy projects management
  ([screenshot](https://gitlab.gnome.org/swilmet/enter-tex-extra/-/raw/main/screenshots/screenshot-project.png)).

- Spell-checking.
- Jump to the associated position between the .tex and the PDF with Ctrl+click.
- Some menus and toolbars with the principal LaTeX commands.
- Integrated file browser.
- All common things found in a text editor.

The idea of Enter TeX is to always deal directly with the TeX code, while
simplifying as most as possible the writing of this TeX code. This permits to
concentrate on the content and the structure of the document.

More information
----------------

[More information about Enter TeX](docs/more-information.md).
