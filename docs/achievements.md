Achievements
============

Noteworthy achievements.

- Version 2.10: implement higher-level search and replace API in GtkSourceView,
  and use it.

- Version 3.16: much improved build tools feature, with a rewrite in C by
  creating the internal liblatexila (now called Gtex).

- Version 3.18: rewrite the LaTeX templates feature in C.

- Versions 3.18-3.20: create the gspell library and use it instead of GtkSpell,
  to improve the spell-checking.

- Versions 3.24-3.26: start to create the Tepl library and use it.

- Version 3.28: rename LaTeXila to GNOME LaTeX (all essential things done).

- Version 3.28: create the Amtk library and start to use it to progressively
  move away from GtkAction/GtkUIManager (deprecated) and use GAction instead.
  This migration is still not finished. While migrating, some code is rewritten
  in C.

- Version 3.42: replace last `*.ui` file by code.

- Version 3.47:
  - Migrate from Autotools to Meson.
  - Rename GNOME LaTeX to Enter TeX.
  - Rename the C code namespace from Latexila to Gtex.
