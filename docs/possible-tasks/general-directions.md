General directions
==================

- Staying with a traditional UI (menubar, toolbars, statusbar).

- Staying with GTK 3, to benefit from a better support of a traditional UI.

- Develop/remove features according to the developer's actual needs.

- Progressively rewrite the code from Vala to C.
