New app icon
============

The current icon is based on a previous icon from gedit.

See: https://gedit-text-editor.org/blog/2024-04-26-change-of-icons-and-logo-for-gedit.html
