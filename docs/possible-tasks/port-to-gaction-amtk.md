Port to GAction and libgedit-amtk
=================================

To get rid of the use of GtkAction and GtkUIManager, which are now deprecated in
GTK 3, and which have been removed in GTK 4.

Misc notes
----------

- GtkRadioMenuItem is used only in: Documents menu. So it can be handled
  separately.

- It _is_ possible to create some menu/toolbar items with Amtk and keep the rest
  with GtkUIManager, during the migration. At the time of writing, the LaTeX and
  Math menus have been ported, while most of the rest isn't.

- Note that there is also `amtk_utils_create_gtk_action()` and other utils
  functions in Amtk to ease the transition.

Todo-list
---------

- Create GAction's, port GtkActionEntry's to AmtkActionInfoEntry's and use
  `amtk_utils_create_gtk_action()`.

- Port toolbars to libgedit-amtk.

- Port the menu to libgedit-amtk.

- Do a maximum in C and in libgedit-amtk or libgedit-tepl.
