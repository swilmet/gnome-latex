Rework personal templates system
================================

Provide a **directory** where personal templates are present.

To create a new document from a personal template, choose a file from the
directory and copy its content.

But what about copying also a .gitignore file (for instance)?

Example workflow
----------------

Personal template:
- article-en.tex
- .gitignore

Create from that a directory with the name of the article:

```
article-name/article-name.tex
article-name/.gitignore
```

With the .gitignore file containing, among other lines:

```
/article-name.pdf
```

Questions
---------

Is it worth implementing a GUI for the "Example workflow"? How to keep things at
the same time simple **and** useful (for me at least), and ideally suitable for
a wide range of TeX users?

Saying: "use the terminal or a file manager instead", plus manually adapting the
.gitignore file, is not really a solution. At this point the whole Personal
Templates feature can be removed from the GUI altogether.

Also, what about doing a git clone of a repo full of templates, and "importing"
it into Enter TeX? (for those users who find that useful).

What about assigning different icons to personal templates, the same as for the
default templates? (article, presentation, letter, etc).

Perhaps a flexible system should be implemented. For each template, a way to
instantiate it. Not only a simple *.tex file, but also additional associated
data.

Decouple the creation of the directory plus the .gitignore file from the
template instantiation? That way templates remain simple *.tex files. The user
could enter a document name as an optional step in order to create the directory
structure for Git. As always, make that configurable.
