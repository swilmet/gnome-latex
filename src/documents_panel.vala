/* SPDX-FileCopyrightText: 2010, 2011, 2017 Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

using Gtk;

// TODO: rename this class to SomethingNotebook, for instance DocumentsNotebook.
// In gedit the DocumentsPanel is the list of documents in the left side panel.
public class DocumentsPanel : Tepl.Notebook
{
    public signal void right_click (Gdk.EventButton event);

    private unowned MainWindow main_window;

    public DocumentsPanel (MainWindow main_window)
    {
        this.main_window = main_window;
    }

    public void add_tab (DocumentTab tab, int position, bool jump_to)
    {
        EventBox event_box = new EventBox ();
        event_box.set_visible_window (false);

        TabLabel tab_label = new TabLabel (tab);
        tab_label.show ();
        event_box.add (tab_label);

        event_box.button_press_event.connect ((event) =>
        {
            // right click
            if (event.button == 3)
            {
                set_current_page (page_num (tab));

                // show popup menu
                right_click (event);
            }

            return false;
        });

        int page_pos = this.insert_page (tab, event_box, position);
        this.set_tab_reorderable (tab, true);
        if (jump_to)
            this.set_current_page (page_pos);
    }

    public void remove_tab (DocumentTab tab)
    {
        // automatic clean-up build files
        GLib.Settings settings =
            new GLib.Settings ("org.gnome.enter_tex.preferences.latex");

        if (settings.get_boolean ("no-confirm-clean")
            && settings.get_boolean ("automatic-clean"))
        {
            CleanBuildFiles build_files = new CleanBuildFiles (main_window, tab.document);
            build_files.clean ();
        }

        int pos = page_num (tab);
        remove_page (pos);
    }

    public void remove_all_tabs ()
    {
        while (true)
        {
            int n = get_current_page ();
            if (n == -1)
                break;
            DocumentTab tab = (DocumentTab) get_nth_page (n);
            remove_tab (tab);
        }
    }
}
