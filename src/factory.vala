/* SPDX-FileCopyrightText: 2017-2024 Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class Factory : Gtex.Factory
{
    public Factory ()
    {
    }

    public override unowned Gtk.ApplicationWindow? create_main_window (Gtk.Application gtk_app)
    {
        return_val_if_fail (gtk_app is GlatexApp, null);
        GlatexApp app = gtk_app as GlatexApp;

        MainWindow? active_main_window = app.get_active_main_window ();
        if (active_main_window != null)
            active_main_window.save_state ();

        bool first_window = active_main_window == null;

        MainWindow new_window = new MainWindow (app);
        if (first_window)
            reopen_files (app);

        return new_window.ref () as MainWindow;
    }

    private void reopen_files (GlatexApp app)
    {
        GLib.Settings editor_settings =
            new GLib.Settings ("org.gnome.enter_tex.preferences.editor");

        if (editor_settings.get_boolean ("reopen-files"))
        {
            GLib.Settings window_settings =
                new GLib.Settings ("org.gnome.enter_tex.state.window");

            string[] uris = window_settings.get_strv ("documents");
            File[] files = {};
            foreach (string uri in uris)
            {
                if (0 < uri.length)
                    files += File.new_for_uri (uri);
            }

            app.open_documents (files);
        }
    }
}
