/* SPDX-FileCopyrightText: 2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "gtex-build-view.h"

G_BEGIN_DECLS

GtkWidget *gtex_bottom_panel_new (GtexBuildView *build_view,
				  GtkToolbar    *toolbar);

G_END_DECLS
