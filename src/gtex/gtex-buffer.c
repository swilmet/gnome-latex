/* SPDX-FileCopyrightText: 2020-2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * SECTION:buffer
 * @title: GtexBuffer
 * @short_description: Additional #TeplBuffer functions
 *
 * Additional #TeplBuffer functions.
 */

#include "gtex-buffer.h"
#include "gtex-settings.h"

/**
 * gtex_buffer_setup:
 * @buffer: a #TeplBuffer.
 *
 * Setups a #TeplBuffer for this application.
 */
void
gtex_buffer_setup (TeplBuffer *buffer)
{
	GtexSettings *settings;
	GSettings *editor_settings;

	g_return_if_fail (GTK_SOURCE_IS_BUFFER (buffer));

	settings = gtex_settings_get_singleton ();
	editor_settings = gtex_settings_peek_editor_settings (settings);

	g_settings_bind (editor_settings, "bracket-matching",
			 buffer, "highlight-matching-brackets",
			 G_SETTINGS_BIND_GET);

	tepl_buffer_connect_style_scheme_settings (buffer);
}

static void
comment_line (GtkTextBuffer *buffer,
	      gint           line_num)
{
	GtkTextIter iter;

	gtk_text_buffer_get_iter_at_line (buffer, &iter, line_num);
	g_return_if_fail (gtk_text_iter_get_line (&iter) == line_num);

	if (gtk_text_iter_ends_line (&iter))
	{
		/* Don't insert a trailing space. */
		gtk_text_buffer_insert (buffer, &iter, "%", -1);
	}
	else
	{
		gtk_text_buffer_insert (buffer, &iter, "% ", -1);
	}
}

/**
 * gtex_buffer_comment_lines:
 * @buffer: a #GtkTextBuffer.
 * @start_iter: a #GtkTextIter.
 * @end_iter: (nullable): a #GtkTextIter, or %NULL to comment only the line at
 *   @start_iter.
 *
 * Comments the lines between @start_iter and @end_iter included.
 *
 * If @end_iter is %NULL, only a single line is commented.
 */
void
gtex_buffer_comment_lines (GtkTextBuffer     *buffer,
			   const GtkTextIter *start_iter,
			   const GtkTextIter *end_iter)
{
	GtkTextIter my_start_iter;
	GtkTextIter my_end_iter;
	gint start_line;
	gint end_line;
	gint line_num;

	g_return_if_fail (GTK_IS_TEXT_BUFFER (buffer));
	g_return_if_fail (start_iter != NULL);

	my_start_iter = *start_iter;
	my_end_iter = end_iter != NULL ? *end_iter : *start_iter;

	gtk_text_iter_order (&my_start_iter, &my_end_iter);

	start_line = gtk_text_iter_get_line (&my_start_iter);
	end_line = gtk_text_iter_get_line (&my_end_iter);

	gtk_text_buffer_begin_user_action (buffer);

	for (line_num = start_line; line_num <= end_line; line_num++)
	{
		comment_line (buffer, line_num);
	}

	gtk_text_buffer_end_user_action (buffer);
}

/**
 * gtex_buffer_comment_selected_lines:
 * @buffer: a #GtkTextBuffer.
 *
 * Comments the selected lines.
 */
void
gtex_buffer_comment_selected_lines (GtkTextBuffer *buffer)
{
	GtkTextIter start;
	GtkTextIter end;

	g_return_if_fail (GTK_IS_TEXT_BUFFER (buffer));

	gtk_text_buffer_get_selection_bounds (buffer, &start, &end);
	gtex_buffer_comment_lines (buffer, &start, &end);
}

static void
uncomment_line (GtkTextBuffer *buffer,
		gint           line_num)
{
	GtkTextIter iter;
	GtkTextIter deletion_start;
	GtkTextIter deletion_end;
	gboolean found = FALSE;

	gtk_text_buffer_get_iter_at_line (buffer, &iter, line_num);
	g_return_if_fail (gtk_text_iter_get_line (&iter) == line_num);

	while (TRUE)
	{
		gunichar ch;

		ch = gtk_text_iter_get_char (&iter);

		if (ch == '%')
		{
			gunichar next_ch;

			deletion_start = iter;
			deletion_end = iter;

			gtk_text_iter_forward_char (&deletion_end);
			next_ch = gtk_text_iter_get_char (&deletion_end);

			if (next_ch == ' ')
			{
				gtk_text_iter_forward_char (&deletion_end);
			}

			found = TRUE;
			break;
		}

		if (ch != ' ' && ch != '\t')
		{
			break;
		}

		gtk_text_iter_forward_char (&iter);
	}

	if (found)
	{
		gtk_text_buffer_delete (buffer, &deletion_start, &deletion_end);
	}
}

/**
 * gtex_buffer_uncomment_selected_lines:
 * @buffer: a #GtkTextBuffer.
 *
 * Uncomments the selected lines.
 */
void
gtex_buffer_uncomment_selected_lines (GtkTextBuffer *buffer)
{
	GtkTextIter start;
	GtkTextIter end;
	gint start_line;
	gint end_line;
	gint line_num;

	g_return_if_fail (GTK_IS_TEXT_BUFFER (buffer));

	gtk_text_buffer_get_selection_bounds (buffer, &start, &end);

	start_line = gtk_text_iter_get_line (&start);
	end_line = gtk_text_iter_get_line (&end);

	gtk_text_buffer_begin_user_action (buffer);

	for (line_num = start_line; line_num <= end_line; line_num++)
	{
		uncomment_line (buffer, line_num);
	}

	gtk_text_buffer_end_user_action (buffer);
}
