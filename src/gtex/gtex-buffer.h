/* SPDX-FileCopyrightText: 2020-2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <tepl/tepl.h>

G_BEGIN_DECLS

void	gtex_buffer_setup			(TeplBuffer *buffer);

void	gtex_buffer_comment_lines		(GtkTextBuffer     *buffer,
						 const GtkTextIter *start_iter,
						 const GtkTextIter *end_iter);

void	gtex_buffer_comment_selected_lines	(GtkTextBuffer *buffer);

void	gtex_buffer_uncomment_selected_lines	(GtkTextBuffer *buffer);

G_END_DECLS
