/* SPDX-FileCopyrightText: 2014 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "gtex-build-tools.h"

G_BEGIN_DECLS

#define GTEX_TYPE_BUILD_TOOLS_DEFAULT             (gtex_build_tools_default_get_type ())
#define GTEX_BUILD_TOOLS_DEFAULT(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_BUILD_TOOLS_DEFAULT, GtexBuildToolsDefault))
#define GTEX_BUILD_TOOLS_DEFAULT_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_BUILD_TOOLS_DEFAULT, GtexBuildToolsDefaultClass))
#define GTEX_IS_BUILD_TOOLS_DEFAULT(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_BUILD_TOOLS_DEFAULT))
#define GTEX_IS_BUILD_TOOLS_DEFAULT_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_BUILD_TOOLS_DEFAULT))
#define GTEX_BUILD_TOOLS_DEFAULT_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_BUILD_TOOLS_DEFAULT, GtexBuildToolsDefaultClass))

typedef struct _GtexBuildToolsDefault        GtexBuildToolsDefault;
typedef struct _GtexBuildToolsDefaultClass   GtexBuildToolsDefaultClass;
typedef struct _GtexBuildToolsDefaultPrivate GtexBuildToolsDefaultPrivate;

struct _GtexBuildToolsDefault
{
	GtexBuildTools parent;

	GtexBuildToolsDefaultPrivate *priv;
};

struct _GtexBuildToolsDefaultClass
{
	GtexBuildToolsClass parent_class;
};

GType			gtex_build_tools_default_get_type	(void);

GtexBuildToolsDefault *	gtex_build_tools_default_get_instance	(void);

G_END_DECLS
