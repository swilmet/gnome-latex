/* SPDX-FileCopyrightText: 2014-2020 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * SECTION:build-tools-personal
 * @title: GtexBuildToolsPersonal
 * @short_description: Personal build tools
 *
 * The #GtexBuildToolsPersonal singleton class represents the personal build
 * tools. The personal build tools can be entirely modified. The XML file can be
 * saved with gtex_build_tools_personal_save().
 */

#include "gtex-build-tools-personal.h"
#include <tepl/tepl.h>
#include "gtex-build-tool.h"
#include "gtex-utils.h"

struct _GtexBuildToolsPersonalPrivate
{
	/* Used for saving */
	GString *xml_file_contents;

	guint modified : 1;
};

G_DEFINE_TYPE_WITH_PRIVATE (GtexBuildToolsPersonal, gtex_build_tools_personal, GTEX_TYPE_BUILD_TOOLS)

static void
gtex_build_tools_personal_finalize (GObject *object)
{
	GtexBuildToolsPersonal *build_tools = GTEX_BUILD_TOOLS_PERSONAL (object);

	if (build_tools->priv->xml_file_contents != NULL)
	{
		g_string_free (build_tools->priv->xml_file_contents, TRUE);
		build_tools->priv->xml_file_contents = NULL;
	}

	G_OBJECT_CLASS (gtex_build_tools_personal_parent_class)->finalize (object);
}

static void
gtex_build_tools_personal_class_init (GtexBuildToolsPersonalClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = gtex_build_tools_personal_finalize;
}

static GFile *
get_xml_file (void)
{
	return g_file_new_build_filename (g_get_user_config_dir (),
					  "enter-tex",
					  "build_tools.xml",
					  NULL);
}

static void
modified_cb (GtexBuildToolsPersonal *build_tools)
{
	build_tools->priv->modified = TRUE;
}

static void
gtex_build_tools_personal_init (GtexBuildToolsPersonal *build_tools)
{
	GFile *xml_file;

	build_tools->priv = gtex_build_tools_personal_get_instance_private (build_tools);

	g_signal_connect (build_tools,
			  "modified",
			  G_CALLBACK (modified_cb),
			  NULL);

	xml_file = get_xml_file ();
	gtex_build_tools_load (GTEX_BUILD_TOOLS (build_tools), xml_file);
	g_object_unref (xml_file);
}

/**
 * gtex_build_tools_personal_get_instance:
 *
 * Gets the instance of the #GtexBuildToolsPersonal singleton.
 *
 * Returns: (transfer none): the instance of #GtexBuildToolsPersonal.
 */
GtexBuildToolsPersonal *
gtex_build_tools_personal_get_instance (void)
{
	static GtexBuildToolsPersonal *instance = NULL;

	if (instance == NULL)
	{
		instance = g_object_new (GTEX_TYPE_BUILD_TOOLS_PERSONAL, NULL);
	}

	return instance;
}

static void
save_cb (GFile                  *xml_file,
	 GAsyncResult           *result,
	 GtexBuildToolsPersonal *build_tools)
{
	GError *error = NULL;

	g_file_replace_contents_finish (xml_file, result, NULL, &error);

	if (error != NULL)
	{
		g_warning ("Error while saving the personal build tools: %s",
			   error->message);
		g_error_free (error);
	}
	else
	{
		build_tools->priv->modified = FALSE;
	}

	g_string_free (build_tools->priv->xml_file_contents, TRUE);
	build_tools->priv->xml_file_contents = NULL;

	g_object_unref (build_tools);
	g_application_release (g_application_get_default ());
}

/**
 * gtex_build_tools_personal_save:
 * @build_tools: the #GtexBuildToolsPersonal instance.
 *
 * Saves asynchronously the personal build tools into the XML file.
 */
void
gtex_build_tools_personal_save (GtexBuildToolsPersonal *build_tools)
{
	GtexBuildTools *build_tools_parent = GTEX_BUILD_TOOLS (build_tools);
	GString *contents;
	GList *cur_build_tool;
	GFile *xml_file;
	GError *error = NULL;

	g_return_if_fail (GTEX_IS_BUILD_TOOLS_PERSONAL (build_tools));

	if (!build_tools->priv->modified ||
	    build_tools->priv->xml_file_contents != NULL)
	{
		return;
	}

	contents = g_string_new ("<tools>");
	build_tools->priv->xml_file_contents = contents;

	for (cur_build_tool = build_tools_parent->build_tools;
	     cur_build_tool != NULL;
	     cur_build_tool = cur_build_tool->next)
	{
		GtexBuildTool *build_tool = cur_build_tool->data;
		gchar *build_tool_xml = gtex_build_tool_to_xml (build_tool);

		g_string_append (contents, build_tool_xml);
		g_free (build_tool_xml);
	}

	g_string_append (contents, "</tools>\n");

	xml_file = get_xml_file ();

	tepl_utils_create_parent_directories (xml_file, NULL, &error);

	if (error == NULL)
	{
		/* Avoid finalization of build_tools during the async operation. And keep the
		 * application running.
		 */
		g_object_ref (build_tools);
		g_application_hold (g_application_get_default ());

		g_file_replace_contents_async (xml_file,
					       contents->str,
					       contents->len,
					       NULL,
					       TRUE, /* make a backup */
					       G_FILE_CREATE_NONE,
					       NULL,
					       (GAsyncReadyCallback) save_cb,
					       build_tools);
	}
	else
	{
		g_warning ("Error while saving the personal build tools: %s",
			   error->message);
		g_error_free (error);
	}

	g_object_unref (xml_file);
}

/**
 * gtex_build_tools_personal_move_up:
 * @build_tools: the #GtexBuildToolsPersonal instance.
 * @tool_num: the build tool position in the list.
 *
 * Move a build tool up. The first build tool is at the top.
 */
void
gtex_build_tools_personal_move_up (GtexBuildToolsPersonal *build_tools,
				   guint                   tool_num)
{
	GtexBuildTools *build_tools_parent = GTEX_BUILD_TOOLS (build_tools);
	GList *node;
	GList *prev_node;

	g_return_if_fail (GTEX_IS_BUILD_TOOLS_PERSONAL (build_tools));

	node = g_list_nth (build_tools_parent->build_tools, tool_num);
	g_return_if_fail (node != NULL);

	prev_node = node->prev;
	g_return_if_fail (prev_node != NULL);

	build_tools_parent->build_tools = g_list_remove_link (build_tools_parent->build_tools, node);

	build_tools_parent->build_tools = g_list_insert_before (build_tools_parent->build_tools,
								prev_node,
								node->data);

	g_list_free (node);

	g_signal_emit_by_name (build_tools, "modified");
}

/**
 * gtex_build_tools_personal_move_down:
 * @build_tools: the #GtexBuildToolsPersonal instance.
 * @tool_num: the build tool position in the list.
 *
 * Move a build tool down. The first build tool is at the top.
 */
void
gtex_build_tools_personal_move_down (GtexBuildToolsPersonal *build_tools,
				     guint                   tool_num)
{
	GtexBuildTools *build_tools_parent = GTEX_BUILD_TOOLS (build_tools);
	GList *node;
	GList *next_node;

	g_return_if_fail (GTEX_IS_BUILD_TOOLS_PERSONAL (build_tools));

	node = g_list_nth (build_tools_parent->build_tools, tool_num);
	g_return_if_fail (node != NULL);

	next_node = node->next;
	g_return_if_fail (next_node != NULL);

	build_tools_parent->build_tools = g_list_remove_link (build_tools_parent->build_tools, node);

	build_tools_parent->build_tools = g_list_insert_before (build_tools_parent->build_tools,
								next_node->next,
								node->data);

	g_list_free (node);

	g_signal_emit_by_name (build_tools, "modified");
}

/**
 * gtex_build_tools_personal_delete:
 * @build_tools: the #GtexBuildToolsPersonal instance.
 * @tool_num: the build tool position in the list.
 *
 * Deletes a build tool.
 */
void
gtex_build_tools_personal_delete (GtexBuildToolsPersonal *build_tools,
				  guint                   tool_num)
{
	GtexBuildTools *build_tools_parent = GTEX_BUILD_TOOLS (build_tools);
	GList *node;

	g_return_if_fail (GTEX_IS_BUILD_TOOLS_PERSONAL (build_tools));

	node = g_list_nth (build_tools_parent->build_tools, tool_num);
	g_return_if_fail (node != NULL);

	build_tools_parent->build_tools = g_list_remove_link (build_tools_parent->build_tools, node);

	g_list_free_full (node, g_object_unref);

	g_signal_emit_by_name (build_tools, "modified");
}

/**
 * gtex_build_tools_personal_add:
 * @build_tools: the #GtexBuildToolsPersonal instance.
 * @new_build_tool: the new build tool object.
 *
 * Append the new build tool at the end of the list.
 */
void
gtex_build_tools_personal_add (GtexBuildToolsPersonal *build_tools,
			       GtexBuildTool          *new_build_tool)
{
	GtexBuildTools *build_tools_parent = GTEX_BUILD_TOOLS (build_tools);

	g_return_if_fail (GTEX_IS_BUILD_TOOLS_PERSONAL (build_tools));

	build_tools_parent->build_tools = g_list_append (build_tools_parent->build_tools,
							 new_build_tool);

	g_object_ref (new_build_tool);

	g_signal_emit_by_name (build_tools, "modified");
}

/**
 * gtex_build_tools_personal_insert:
 * @build_tools: the #GtexBuildToolsPersonal instance.
 * @new_build_tool: the new build tool object.
 * @position: the position in the list where to insert the new build tool.
 *
 * Inserts a new build tool at a given position.
 */
void
gtex_build_tools_personal_insert (GtexBuildToolsPersonal *build_tools,
				  GtexBuildTool          *new_build_tool,
				  guint                   position)
{
	GtexBuildTools *build_tools_parent = GTEX_BUILD_TOOLS (build_tools);

	g_return_if_fail (GTEX_IS_BUILD_TOOLS_PERSONAL (build_tools));

	build_tools_parent->build_tools = g_list_insert (build_tools_parent->build_tools,
							 new_build_tool,
							 position);

	g_object_ref (new_build_tool);

	g_signal_emit_by_name (build_tools, "modified");
}

/**
 * gtex_build_tools_personal_replace:
 * @build_tools: the #GtexBuildToolsPersonal instance.
 * @new_build_tool: the new build tool object.
 * @position: the position in the list where to replace the build tool. The old
 * build tool located at @position will be unreffed.
 *
 * Replaces a build tool.
 */
void
gtex_build_tools_personal_replace (GtexBuildToolsPersonal *build_tools,
				   GtexBuildTool          *new_build_tool,
				   guint                   position)
{
	GtexBuildTools *build_tools_parent = GTEX_BUILD_TOOLS (build_tools);
	GList *node;

	g_return_if_fail (GTEX_IS_BUILD_TOOLS_PERSONAL (build_tools));

	node = g_list_nth (build_tools_parent->build_tools, position);
	g_return_if_fail (node != NULL);

	if (node->data != new_build_tool)
	{
		g_object_unref (node->data);
		node->data = g_object_ref (new_build_tool);

		g_signal_emit_by_name (build_tools, "modified");
	}
}
