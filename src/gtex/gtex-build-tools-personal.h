/* SPDX-FileCopyrightText: 2014 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "gtex-build-tools.h"

G_BEGIN_DECLS

#define GTEX_TYPE_BUILD_TOOLS_PERSONAL             (gtex_build_tools_personal_get_type ())
#define GTEX_BUILD_TOOLS_PERSONAL(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_BUILD_TOOLS_PERSONAL, GtexBuildToolsPersonal))
#define GTEX_BUILD_TOOLS_PERSONAL_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_BUILD_TOOLS_PERSONAL, GtexBuildToolsPersonalClass))
#define GTEX_IS_BUILD_TOOLS_PERSONAL(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_BUILD_TOOLS_PERSONAL))
#define GTEX_IS_BUILD_TOOLS_PERSONAL_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_BUILD_TOOLS_PERSONAL))
#define GTEX_BUILD_TOOLS_PERSONAL_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_BUILD_TOOLS_PERSONAL, GtexBuildToolsPersonalClass))

typedef struct _GtexBuildToolsPersonal        GtexBuildToolsPersonal;
typedef struct _GtexBuildToolsPersonalClass   GtexBuildToolsPersonalClass;
typedef struct _GtexBuildToolsPersonalPrivate GtexBuildToolsPersonalPrivate;

struct _GtexBuildToolsPersonal
{
	GtexBuildTools parent;

	GtexBuildToolsPersonalPrivate *priv;
};

struct _GtexBuildToolsPersonalClass
{
	GtexBuildToolsClass parent_class;
};

GType		gtex_build_tools_personal_get_type	(void);

GtexBuildToolsPersonal *
		gtex_build_tools_personal_get_instance	(void);

void		gtex_build_tools_personal_save		(GtexBuildToolsPersonal *build_tools);

void		gtex_build_tools_personal_move_up	(GtexBuildToolsPersonal *build_tools,
							 guint                   tool_num);

void		gtex_build_tools_personal_move_down	(GtexBuildToolsPersonal *build_tools,
							 guint                   tool_num);

void		gtex_build_tools_personal_delete	(GtexBuildToolsPersonal *build_tools,
							 guint                   tool_num);

void		gtex_build_tools_personal_add		(GtexBuildToolsPersonal *build_tools,
							 GtexBuildTool          *new_build_tool);

void		gtex_build_tools_personal_insert	(GtexBuildToolsPersonal *build_tools,
							 GtexBuildTool          *new_build_tool,
							 guint                   position);

void		gtex_build_tools_personal_replace	(GtexBuildToolsPersonal *build_tools,
							 GtexBuildTool          *new_build_tool,
							 guint                   position);

G_END_DECLS
