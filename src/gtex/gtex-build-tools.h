/* SPDX-FileCopyrightText: 2014 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>
#include "gtex-build-tool.h"

G_BEGIN_DECLS

#define GTEX_TYPE_BUILD_TOOLS             (gtex_build_tools_get_type ())
#define GTEX_BUILD_TOOLS(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_BUILD_TOOLS, GtexBuildTools))
#define GTEX_BUILD_TOOLS_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_BUILD_TOOLS, GtexBuildToolsClass))
#define GTEX_IS_BUILD_TOOLS(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_BUILD_TOOLS))
#define GTEX_IS_BUILD_TOOLS_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_BUILD_TOOLS))
#define GTEX_BUILD_TOOLS_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_BUILD_TOOLS, GtexBuildToolsClass))

typedef struct _GtexBuildTools        GtexBuildTools;
typedef struct _GtexBuildToolsClass   GtexBuildToolsClass;
typedef struct _GtexBuildToolsPrivate GtexBuildToolsPrivate;

/**
 * GtexBuildTools:
 * @build_tools: (element-type GtexBuildTool): a list of
 * #GtexBuildTool's. External code should just read the list, not modify it.
 */
struct _GtexBuildTools
{
	/*< private >*/
	GObject parent;

	/*< public >*/
	GList *build_tools;

	/*< private >*/
	GtexBuildToolsPrivate *priv;
};

struct _GtexBuildToolsClass
{
	GObjectClass parent_class;

	/* When the XML file is not found. */
	void (* handle_not_found_error) (GtexBuildTools *build_tools,
					 GFile          *xml_file,
					 GError         *error);
};

GType		gtex_build_tools_get_type	(void);

void		gtex_build_tools_load		(GtexBuildTools *build_tools,
						 GFile          *xml_file);

GtexBuildTool *	gtex_build_tools_nth		(GtexBuildTools *build_tools,
						 guint           tool_num);

void		gtex_build_tools_set_enabled	(GtexBuildTools *build_tools,
						 guint           tool_num,
						 gboolean        enabled);

G_END_DECLS
