/* SPDX-FileCopyrightText: 2017-2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

void		_gtex_commands_latex_add_action_infos	(GtkApplication *gtk_app);

void		_gtex_commands_latex_add_actions	(GtkApplicationWindow *gtk_window);

GtkToolbar *	gtex_commands_create_edit_toolbar	(GtkApplicationWindow *gtk_window);

GtkMenu *	gtex_commands_create_latex_menu		(GtkApplicationWindow *gtk_window);

GtkMenu *	gtex_commands_create_math_menu		(GtkApplicationWindow *gtk_window);

G_END_DECLS
