/* SPDX-FileCopyrightText: 2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gtex-commands.h"
#include "gtex-commands-edit.h"
#include "gtex-commands-latex.h"

/**
 * SECTION:commands
 * @title: GtexCommands
 * @short_description: Actions, menus and toolbars
 */

/**
 * gtex_commands_add_action_infos:
 * @app: the #GtkApplication instance.
 *
 * Creates the #AmtkActionInfo's and adds them to the #AmtkActionInfoStore
 * returned by tepl_application_get_app_action_info_store().
 */
void
gtex_commands_add_action_infos (GtkApplication *app)
{
	g_return_if_fail (GTK_IS_APPLICATION (app));

	_gtex_commands_edit_add_action_infos (app);
	_gtex_commands_latex_add_action_infos (app);
}

/**
 * gtex_commands_add_actions:
 * @window: a #GtkApplicationWindow.
 *
 * Creates the #GAction's and adds them to @window.
 */
void
gtex_commands_add_actions (GtkApplicationWindow *window)
{
	g_return_if_fail (GTK_IS_APPLICATION_WINDOW (window));

	_gtex_commands_edit_add_actions (window);
	_gtex_commands_latex_add_actions (window);
}
