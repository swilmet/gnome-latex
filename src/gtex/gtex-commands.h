/* SPDX-FileCopyrightText: 2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

void	gtex_commands_add_action_infos	(GtkApplication *app);

void	gtex_commands_add_actions	(GtkApplicationWindow *window);

G_END_DECLS
