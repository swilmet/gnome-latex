/* SPDX-FileCopyrightText: 2022 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

void		_gtex_dirs_init			(void);

void		_gtex_dirs_shutdown		(void);

const gchar *	gtex_dirs_get_app_data_dir	(void);

const gchar *	gtex_dirs_get_app_locale_dir	(void);

G_END_DECLS
