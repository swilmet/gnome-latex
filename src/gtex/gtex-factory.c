/* SPDX-FileCopyrightText: 2020-2024 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gtex-factory.h"
#include "gtex-prefs.h"

/**
 * SECTION:factory
 * @title: GtexFactory
 * @short_description: #TeplAbstractFactory subclass
 *
 * #GtexFactory is a #TeplAbstractFactory subclass to implement some virtual
 * functions for this application.
 */

G_DEFINE_TYPE (GtexFactory, gtex_factory, TEPL_TYPE_ABSTRACT_FACTORY)

static GFile *
gtex_factory_create_metadata_manager_file (TeplAbstractFactory *factory)
{
	return g_file_new_build_filename (g_get_user_data_dir (),
					  "enter-tex",
					  "enter-tex-metadata.xml",
					  NULL);
}

static TeplPrefsDialog *
gtex_factory_create_prefs_dialog (TeplAbstractFactory *factory)
{
	TeplPrefsDialog *dialog;

	dialog = TEPL_ABSTRACT_FACTORY_CLASS (gtex_factory_parent_class)->create_prefs_dialog (factory);

	gtex_prefs_fill_dialog (dialog);

	return dialog;
}

static void
gtex_factory_class_init (GtexFactoryClass *klass)
{
	TeplAbstractFactoryClass *factory_class = TEPL_ABSTRACT_FACTORY_CLASS (klass);

	factory_class->create_metadata_manager_file = gtex_factory_create_metadata_manager_file;
	factory_class->create_prefs_dialog = gtex_factory_create_prefs_dialog;
}

static void
gtex_factory_init (GtexFactory *factory)
{
}

/**
 * gtex_factory_new:
 *
 * Returns: a new #GtexFactory object.
 */
GtexFactory *
gtex_factory_new (void)
{
	return g_object_new (GTEX_TYPE_FACTORY, NULL);
}
