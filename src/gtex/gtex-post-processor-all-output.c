/* SPDX-FileCopyrightText: 2014 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * SECTION:post-processor-all-output
 * @title: GtexPostProcessorAllOutput
 * @short_description: all-output post-processor
 *
 * A post-processor that keeps all the output. Nothing is filtered.
 */

#include "gtex-post-processor-all-output.h"
#include "gtex-build-view.h"

struct _GtexPostProcessorAllOutputPrivate
{
	GQueue *messages;
};

G_DEFINE_TYPE_WITH_PRIVATE (GtexPostProcessorAllOutput,
			    gtex_post_processor_all_output,
			    GTEX_TYPE_POST_PROCESSOR)

static void
gtex_post_processor_all_output_process_line (GtexPostProcessor *post_processor,
					     gchar             *line)
{
	GtexPostProcessorAllOutput *pp = GTEX_POST_PROCESSOR_ALL_OUTPUT (post_processor);

	if (line != NULL)
	{
		GtexBuildMsg *msg;

		msg = gtex_build_msg_new ();
		msg->text = line;
		msg->type = GTEX_BUILD_MSG_TYPE_INFO;

		g_queue_push_tail (pp->priv->messages, msg);
	}
}

static const GList *
gtex_post_processor_all_output_get_messages (GtexPostProcessor *post_processor,
					     gboolean           show_details)
{
	GtexPostProcessorAllOutput *pp = GTEX_POST_PROCESSOR_ALL_OUTPUT (post_processor);

	return pp->priv->messages != NULL ? pp->priv->messages->head : NULL;
}

static GQueue *
gtex_post_processor_all_output_take_messages (GtexPostProcessor *post_processor)
{
	GtexPostProcessorAllOutput *pp = GTEX_POST_PROCESSOR_ALL_OUTPUT (post_processor);
	GQueue *ret;

	ret = pp->priv->messages;
	pp->priv->messages = NULL;

	return ret;
}

static void
gtex_post_processor_all_output_finalize (GObject *object)
{
	GtexPostProcessorAllOutput *pp = GTEX_POST_PROCESSOR_ALL_OUTPUT (object);

	if (pp->priv->messages != NULL)
	{
		g_queue_free_full (pp->priv->messages, (GDestroyNotify) gtex_build_msg_free);
	}

	G_OBJECT_CLASS (gtex_post_processor_all_output_parent_class)->finalize (object);
}

static void
gtex_post_processor_all_output_class_init (GtexPostProcessorAllOutputClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtexPostProcessorClass *post_processor_class = GTEX_POST_PROCESSOR_CLASS (klass);

	object_class->finalize = gtex_post_processor_all_output_finalize;

	post_processor_class->process_line = gtex_post_processor_all_output_process_line;
	post_processor_class->get_messages = gtex_post_processor_all_output_get_messages;
	post_processor_class->take_messages = gtex_post_processor_all_output_take_messages;
}

static void
gtex_post_processor_all_output_init (GtexPostProcessorAllOutput *pp)
{
	pp->priv = gtex_post_processor_all_output_get_instance_private (pp);

	pp->priv->messages = g_queue_new ();
}

/**
 * gtex_post_processor_all_output_new:
 *
 * Returns: a new #GtexPostProcessorAllOutput object.
 */
GtexPostProcessor *
gtex_post_processor_all_output_new (void)
{
	return g_object_new (GTEX_TYPE_POST_PROCESSOR_ALL_OUTPUT, NULL);
}
