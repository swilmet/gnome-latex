/* SPDX-FileCopyrightText: 2014 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "gtex-post-processor.h"

G_BEGIN_DECLS

#define GTEX_TYPE_POST_PROCESSOR_LATEX             (gtex_post_processor_latex_get_type ())
#define GTEX_POST_PROCESSOR_LATEX(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_POST_PROCESSOR_LATEX, GtexPostProcessorLatex))
#define GTEX_POST_PROCESSOR_LATEX_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_POST_PROCESSOR_LATEX, GtexPostProcessorLatexClass))
#define GTEX_IS_POST_PROCESSOR_LATEX(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_POST_PROCESSOR_LATEX))
#define GTEX_IS_POST_PROCESSOR_LATEX_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_POST_PROCESSOR_LATEX))
#define GTEX_POST_PROCESSOR_LATEX_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_POST_PROCESSOR_LATEX, GtexPostProcessorLatexClass))

typedef struct _GtexPostProcessorLatex        GtexPostProcessorLatex;
typedef struct _GtexPostProcessorLatexClass   GtexPostProcessorLatexClass;
typedef struct _GtexPostProcessorLatexPrivate GtexPostProcessorLatexPrivate;

struct _GtexPostProcessorLatex
{
	GtexPostProcessor parent;

	GtexPostProcessorLatexPrivate *priv;
};

struct _GtexPostProcessorLatexClass
{
	GtexPostProcessorClass parent_class;
};

GType			gtex_post_processor_latex_get_type		(void);

GtexPostProcessor *	gtex_post_processor_latex_new			(void);

gint			gtex_post_processor_latex_get_errors_count	(GtexPostProcessorLatex *pp);

G_END_DECLS
