/* SPDX-FileCopyrightText: 2014 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "gtex-post-processor.h"

G_BEGIN_DECLS

#define GTEX_TYPE_POST_PROCESSOR_LATEXMK             (gtex_post_processor_latexmk_get_type ())
#define GTEX_POST_PROCESSOR_LATEXMK(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_POST_PROCESSOR_LATEXMK, GtexPostProcessorLatexmk))
#define GTEX_POST_PROCESSOR_LATEXMK_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_POST_PROCESSOR_LATEXMK, GtexPostProcessorLatexmkClass))
#define GTEX_IS_POST_PROCESSOR_LATEXMK(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_POST_PROCESSOR_LATEXMK))
#define GTEX_IS_POST_PROCESSOR_LATEXMK_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_POST_PROCESSOR_LATEXMK))
#define GTEX_POST_PROCESSOR_LATEXMK_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_POST_PROCESSOR_LATEXMK, GtexPostProcessorLatexmkClass))

typedef struct _GtexPostProcessorLatexmk        GtexPostProcessorLatexmk;
typedef struct _GtexPostProcessorLatexmkClass   GtexPostProcessorLatexmkClass;
typedef struct _GtexPostProcessorLatexmkPrivate GtexPostProcessorLatexmkPrivate;

struct _GtexPostProcessorLatexmk
{
	GtexPostProcessor parent;

	GtexPostProcessorLatexmkPrivate *priv;
};

struct _GtexPostProcessorLatexmkClass
{
	GtexPostProcessorClass parent_class;
};

GType			gtex_post_processor_latexmk_get_type	(void);

GtexPostProcessor *	gtex_post_processor_latexmk_new		(void);

G_END_DECLS
