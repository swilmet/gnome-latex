/* SPDX-FileCopyrightText: 2014 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define GTEX_TYPE_POST_PROCESSOR             (gtex_post_processor_get_type ())
#define GTEX_POST_PROCESSOR(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_POST_PROCESSOR, GtexPostProcessor))
#define GTEX_POST_PROCESSOR_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_POST_PROCESSOR, GtexPostProcessorClass))
#define GTEX_IS_POST_PROCESSOR(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_POST_PROCESSOR))
#define GTEX_IS_POST_PROCESSOR_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_POST_PROCESSOR))
#define GTEX_POST_PROCESSOR_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_POST_PROCESSOR, GtexPostProcessorClass))

typedef struct _GtexPostProcessor        GtexPostProcessor;
typedef struct _GtexPostProcessorClass   GtexPostProcessorClass;
typedef struct _GtexPostProcessorPrivate GtexPostProcessorPrivate;

/**
 * GtexPostProcessorType:
 * @GTEX_POST_PROCESSOR_TYPE_NO_OUTPUT: no output.
 * @GTEX_POST_PROCESSOR_TYPE_ALL_OUTPUT: all output.
 * @GTEX_POST_PROCESSOR_TYPE_LATEX: for a LaTeX command.
 * @GTEX_POST_PROCESSOR_TYPE_LATEXMK: for the latexmk command.
 * @GTEX_POST_PROCESSOR_TYPE_NB_TYPES: number of post-processor types.
 *
 * Types of post-processors.
 */
typedef enum
{
	GTEX_POST_PROCESSOR_TYPE_NO_OUTPUT,
	GTEX_POST_PROCESSOR_TYPE_ALL_OUTPUT,
	GTEX_POST_PROCESSOR_TYPE_LATEX,
	GTEX_POST_PROCESSOR_TYPE_LATEXMK,
	GTEX_POST_PROCESSOR_TYPE_NB_TYPES
} GtexPostProcessorType;

struct _GtexPostProcessor
{
	GObject parent;

	GtexPostProcessorPrivate *priv;
};

struct _GtexPostProcessorClass
{
	GObjectClass parent_class;

	void (* start) (GtexPostProcessor *pp,
			GFile             *file);

	void (* process_line) (GtexPostProcessor *pp,
			       gchar             *line);

	void (* end) (GtexPostProcessor *pp,
		      gboolean           succeeded);

	const GList * (* get_messages) (GtexPostProcessor *pp,
					gboolean           show_details);

	GQueue * (* take_messages) (GtexPostProcessor *pp);
};

GType		gtex_post_processor_get_type		(void);

gboolean	gtex_post_processor_get_type_from_name	(const gchar           *name,
							 GtexPostProcessorType *type);

const gchar *	gtex_post_processor_get_name_from_type	(GtexPostProcessorType type);

void		gtex_post_processor_process_async	(GtexPostProcessor   *pp,
							 GFile               *file,
							 GInputStream        *stream,
							 GCancellable        *cancellable,
							 GAsyncReadyCallback  callback,
							 gpointer             user_data);

void		gtex_post_processor_process_finish	(GtexPostProcessor *pp,
							 GAsyncResult      *result,
							 gboolean           succeeded);

void		gtex_post_processor_start		(GtexPostProcessor *pp,
							 GFile             *file);

void		gtex_post_processor_process_line	(GtexPostProcessor *pp,
							 gchar             *line);

void		gtex_post_processor_end			(GtexPostProcessor *pp,
							 gboolean           succeeded);

const GList *	gtex_post_processor_get_messages	(GtexPostProcessor *pp,
							 gboolean           show_details);

GQueue *	gtex_post_processor_take_messages	(GtexPostProcessor *pp);

G_END_DECLS
