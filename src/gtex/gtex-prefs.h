/* SPDX-FileCopyrightText: 2022-2024 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <tepl/tepl.h>

G_BEGIN_DECLS

void gtex_prefs_fill_dialog (TeplPrefsDialog *dialog);

G_END_DECLS
