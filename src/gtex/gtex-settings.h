/* SPDX-FileCopyrightText: 2020 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define GTEX_TYPE_SETTINGS             (gtex_settings_get_type ())
#define GTEX_SETTINGS(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_SETTINGS, GtexSettings))
#define GTEX_SETTINGS_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_SETTINGS, GtexSettingsClass))
#define GTEX_IS_SETTINGS(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_SETTINGS))
#define GTEX_IS_SETTINGS_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_SETTINGS))
#define GTEX_SETTINGS_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_SETTINGS, GtexSettingsClass))

typedef struct _GtexSettings         GtexSettings;
typedef struct _GtexSettingsClass    GtexSettingsClass;
typedef struct _GtexSettingsPrivate  GtexSettingsPrivate;

struct _GtexSettings
{
	GObject parent;

	GtexSettingsPrivate *priv;
};

struct _GtexSettingsClass
{
	GObjectClass parent_class;
};

GType		gtex_settings_get_type			(void);

GtexSettings *	gtex_settings_get_singleton		(void);

void		_gtex_settings_unref_singleton		(void);

void		_gtex_settings_setup			(GtexSettings *self);

GSettings *	gtex_settings_peek_editor_settings	(GtexSettings *self);

GSettings *	gtex_settings_peek_latex_settings	(GtexSettings *self);

G_END_DECLS
