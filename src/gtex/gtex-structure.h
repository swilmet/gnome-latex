/* SPDX-FileCopyrightText: 2024 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTEX_TYPE_STRUCTURE             (gtex_structure_get_type ())
#define GTEX_STRUCTURE(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_STRUCTURE, GtexStructure))
#define GTEX_STRUCTURE_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_STRUCTURE, GtexStructureClass))
#define GTEX_IS_STRUCTURE(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_STRUCTURE))
#define GTEX_IS_STRUCTURE_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_STRUCTURE))
#define GTEX_STRUCTURE_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_STRUCTURE, GtexStructureClass))

typedef struct _GtexStructure         GtexStructure;
typedef struct _GtexStructureClass    GtexStructureClass;
typedef struct _GtexStructurePrivate  GtexStructurePrivate;

struct _GtexStructure
{
	GtkGrid parent;

	GtexStructurePrivate *priv;
};

struct _GtexStructureClass
{
	GtkGridClass parent_class;
};

GType		gtex_structure_get_type	(void);

GtexStructure *	gtex_structure_new	(void);

G_END_DECLS
