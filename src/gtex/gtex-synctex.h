/* SPDX-FileCopyrightText: 2014 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTEX_TYPE_SYNCTEX             (gtex_synctex_get_type ())
#define GTEX_SYNCTEX(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_SYNCTEX, GtexSynctex))
#define GTEX_SYNCTEX_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_SYNCTEX, GtexSynctexClass))
#define GTEX_IS_SYNCTEX(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_SYNCTEX))
#define GTEX_IS_SYNCTEX_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_SYNCTEX))
#define GTEX_SYNCTEX_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_SYNCTEX, GtexSynctexClass))

typedef struct _GtexSynctex        GtexSynctex;
typedef struct _GtexSynctexClass   GtexSynctexClass;
typedef struct _GtexSynctexPrivate GtexSynctexPrivate;

struct _GtexSynctex
{
	GObject parent;

	GtexSynctexPrivate *priv;
};

struct _GtexSynctexClass
{
	GObjectClass parent_class;
};

GType		gtex_synctex_get_type			(void);

GtexSynctex *	gtex_synctex_get_instance		(void);

void		gtex_synctex_connect_evince_window	(GtexSynctex *synctex,
							 const gchar *pdf_uri);

void		gtex_synctex_forward_search		(GtexSynctex   *synctex,
							 GtkTextBuffer *buffer,
							 GFile         *buffer_location,
							 GFile         *main_tex_file,
							 guint          timestamp);

G_END_DECLS
