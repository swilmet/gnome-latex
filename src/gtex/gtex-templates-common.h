/* SPDX-FileCopyrightText: 2015-2024 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

enum _GtexTemplatesColumn
{
	GTEX_TEMPLATES_COLUMN_PIXBUF_ICON_NAME,

	/* The string stored in the rc file (article, report, ...). For
	* backward-compatibility reasons, this is not the same as PIXBUF_ICON_NAME.
	*/
	GTEX_TEMPLATES_COLUMN_CONFIG_ICON_NAME,

	GTEX_TEMPLATES_COLUMN_NAME,

	/* The *.tex file where is stored the contents. A NULL file is valid for a
	* default template, it means an empty template.
	*/
	GTEX_TEMPLATES_COLUMN_FILE,

	GTEX_TEMPLATES_N_COLUMNS
};

void		gtex_templates_init_store	(GtkListStore *store);

void		gtex_templates_add_template	(GtkListStore *store,
						 const gchar  *name,
						 const gchar  *config_icon_name,
						 GFile        *file);

GtkTreeView *	gtex_templates_get_view		(GtkListStore *store);

G_END_DECLS
