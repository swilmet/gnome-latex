/* SPDX-FileCopyrightText: 2015-2024 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * SECTION:templates-default
 * @title: GtexTemplatesDefault
 * @short_description: Default templates
 * @see_also: #GtexTemplatesPersonal
 *
 * #GtexTemplatesDefault is a singleton class that stores information about
 * default templates. In this application, new documents are created from
 * templates. There are a few default templates available, and personal
 * templates can be created (see #GtexTemplatesPersonal).
 *
 * Each LaTeX user has probably different needs, and has a different set of
 * templates. So it's better to keep a short list of default templates. It would
 * be useless to have a hundred default templates, since anyway they would most
 * probably not fit many users. For example each user has a different preamble,
 * with different packages, configured differently, etc.
 *
 * In the git repository, default templates are located in the data/templates/
 * directory.
 */

#include "gtex-templates-default.h"
#include <glib/gi18n.h>
#include "gtex-dirs.h"
#include "gtex-templates-common.h"

struct _GtexTemplatesDefault
{
	GtkListStore parent;
};

G_DEFINE_TYPE (GtexTemplatesDefault, gtex_templates_default, GTK_TYPE_LIST_STORE)

static void
gtex_templates_default_class_init (GtexTemplatesDefaultClass *klass)
{
}

static void
add_default_template (GtexTemplatesDefault *templates,
		      const gchar          *name,
		      const gchar          *config_icon_name,
		      const gchar          *filename)
{
	GFile *file;

	file = g_file_new_build_filename (gtex_dirs_get_app_data_dir (), "templates", filename, NULL);

	gtex_templates_add_template (GTK_LIST_STORE (templates),
				     name,
				     config_icon_name,
				     file);

	g_object_unref (file);
}

static void
gtex_templates_default_init (GtexTemplatesDefault *templates)
{
	gtex_templates_init_store (GTK_LIST_STORE (templates));

	gtex_templates_add_template (GTK_LIST_STORE (templates),
				     _("Empty"),
				     "empty",
				     NULL);

	add_default_template (templates, _("Article"), "article", "article.tex");
	add_default_template (templates, _("Report"), "report", "report.tex");
	add_default_template (templates, _("Book"), "book", "book.tex");
	add_default_template (templates, _("Letter"), "letter", "letter.tex");
	add_default_template (templates, _("Presentation"), "beamer", "beamer.tex");
}

/**
 * gtex_templates_default_get_instance:
 *
 * Gets the instance of the #GtexTemplatesDefault singleton.
 *
 * Returns: (transfer none): the instance of #GtexTemplatesDefault.
 */
GtexTemplatesDefault *
gtex_templates_default_get_instance (void)
{
	static GtexTemplatesDefault *instance = NULL;

	if (instance == NULL)
	{
		instance = g_object_new (GTEX_TYPE_TEMPLATES_DEFAULT, NULL);
	}

	return instance;
}

/**
 * gtex_templates_default_get_contents:
 * @templates: the #GtexTemplatesDefault instance.
 * @path: the #GtkTreePath of a default template.
 *
 * Gets the contents of a default template.
 *
 * TODO load contents asynchronously.
 *
 * Returns: the default template's contents. Free with g_free().
 */
gchar *
gtex_templates_default_get_contents (GtexTemplatesDefault *templates,
				     GtkTreePath          *path)
{
	GtkTreeIter iter;
	GFile *tex_file = NULL;
	gchar *contents = NULL;
	GError *error = NULL;

	g_return_val_if_fail (GTEX_IS_TEMPLATES_DEFAULT (templates), NULL);

	gtk_tree_model_get_iter (GTK_TREE_MODEL (templates),
				 &iter,
				 path);

	gtk_tree_model_get (GTK_TREE_MODEL (templates),
			    &iter,
			    GTEX_TEMPLATES_COLUMN_FILE, &tex_file,
			    -1);

	if (tex_file == NULL)
	{
		return g_strdup ("");
	}

	g_file_load_contents (tex_file, NULL, &contents, NULL, NULL, &error);

	if (error != NULL)
	{
		g_warning ("Error when loading default template contents: %s", error->message);
		g_clear_error (&error);

		g_clear_pointer (&contents, g_free);
	}

	g_object_unref (tex_file);
	return contents;
}
