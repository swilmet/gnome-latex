/* SPDX-FileCopyrightText: 2015 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTEX_TYPE_TEMPLATES_MANAGE_DIALOG gtex_templates_manage_dialog_get_type ()

G_DECLARE_FINAL_TYPE (GtexTemplatesManageDialog,
		      gtex_templates_manage_dialog,
		      GTEX,
		      TEMPLATES_MANAGE_DIALOG,
		      GtkDialog)

GtkDialog *	gtex_templates_manage_dialog_new	(GtkWindow *parent_window);

G_END_DECLS
