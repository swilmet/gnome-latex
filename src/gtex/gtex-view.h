/* SPDX-FileCopyrightText: 2017-2020 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtksourceview/gtksource.h>

G_BEGIN_DECLS

void	gtex_view_setup			(GtkSourceView *view);

gchar *	gtex_view_get_indentation_style	(GtkSourceView *view);

G_END_DECLS
