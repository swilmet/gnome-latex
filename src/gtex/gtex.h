/* SPDX-FileCopyrightText: 2014-2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* Main header for the Enter TeX "library". Code using the library (e.g. Vala
 * code) should #include only this header.
 */

#pragma once

#include "gtex-enum-types.h"

#include "gtex-app.h"
#include "gtex-bottom-panel.h"
#include "gtex-buffer.h"
#include "gtex-build-job.h"
#include "gtex-build-tool.h"
#include "gtex-build-tools.h"
#include "gtex-build-tools-default.h"
#include "gtex-build-tools-personal.h"
#include "gtex-build-view.h"
#include "gtex-commands.h"
#include "gtex-commands-edit.h"
#include "gtex-commands-latex.h"
#include "gtex-dirs.h"
#include "gtex-factory.h"
#include "gtex-init.h"
#include "gtex-post-processor.h"
#include "gtex-post-processor-all-output.h"
#include "gtex-post-processor-latex.h"
#include "gtex-prefs.h"
#include "gtex-settings.h"
#include "gtex-structure.h"
#include "gtex-structure-types.h"
#include "gtex-synctex.h"
#include "gtex-templates-default.h"
#include "gtex-templates-personal.h"
#include "gtex-templates-dialogs.h"
#include "gtex-templates-manage-dialog.h"
#include "gtex-tree-model-node.h"
#include "gtex-utils.h"
#include "gtex-view.h"
