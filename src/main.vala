/* SPDX-FileCopyrightText: 2010-2020 Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Sébastien Wilmet
 */

int main (string[] argv)
{
    Gtex.init ();
    Factory factory = new Factory ();
    factory.set_singleton ();

    GlatexApp app = new GlatexApp ();
    int status = app.run (argv);

    Gtex.finalize ();
    return status;
}
