/* SPDX-FileCopyrightText: 2012-2025 Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Sébastien Wilmet
 */

using Gtk;

// The Edit menu of a MainWindow

public class MainWindowEdit
{
    private const Gtk.ActionEntry[] _action_entries =
    {
        { "Edit", null, N_("_Edit") },
    };

    private unowned MainWindow _main_window;
    private Gtk.ActionGroup _action_group;

    public MainWindowEdit (MainWindow main_window, UIManager ui_manager)
    {
        _main_window = main_window;

        _action_group = new Gtk.ActionGroup ("EditMenuActionGroup");
        _action_group.set_translation_domain (Config.GETTEXT_PACKAGE);
        _action_group.add_actions (_action_entries, this);

        Amtk.utils_create_gtk_action (main_window, "win.tepl-undo",
            _action_group, "EditUndo");
        Amtk.utils_create_gtk_action (main_window, "win.tepl-redo",
            _action_group, "EditRedo");
        Amtk.utils_create_gtk_action (main_window, "win.tepl-cut",
            _action_group, "EditCut");
        Amtk.utils_create_gtk_action (main_window, "win.tepl-copy",
            _action_group, "EditCopy");
        Amtk.utils_create_gtk_action (main_window, "win.tepl-paste",
            _action_group, "EditPaste");

        ui_manager.insert_action_group (_action_group, 0);
    }
}
