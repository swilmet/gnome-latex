/* SPDX-FileCopyrightText: 2017 Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class TabLabel : Tepl.TabLabel
{
    public TabLabel (DocumentTab tab)
    {
        Object (tab: tab);

        Document doc = tab.get_buffer () as Document;
        doc.notify["project-id"].connect (update_tooltip);
    }

    public override string get_tooltip_markup ()
    {
        string base_tooltip = base.get_tooltip_markup ();

        Document doc = tab.get_buffer () as Document;
        File? location = doc.get_file ().get_location ();
        if (location == null)
            return base_tooltip;

        Project? project = doc.get_project ();
        if (project == null)
            return base_tooltip;

        if (base_tooltip == null)
            base_tooltip = "";

        if (project.main_file.equal (location))
            return base_tooltip + Markup.printf_escaped ("\n<b>%s</b>",
                _("Project main file"));

        return base_tooltip + Markup.printf_escaped ("\n<b>%s</b> %s",
            _("Project main file:"), get_main_file_relative_path ());
    }

    private string? get_main_file_relative_path ()
    {
        Document doc = tab.get_buffer () as Document;
        Project? project = doc.get_project ();
        if (project == null)
            return "";

        File origin = doc.get_file ().get_location ();
        File target = project.main_file;

        return Gtex.utils_get_relative_path (origin, false, target);
    }
}
