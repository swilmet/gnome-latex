/* SPDX-FileCopyrightText: 2010-2020 Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

using Gtk;

namespace Utils
{
    /*************************************************************************/
    // String utilities

    public unowned string? get_string_from_resource (string resource_path)
    {
        try
        {
            Bytes bytes = resources_lookup_data (resource_path, 0);
            return (string) bytes.get_data ();
        }
        catch (Error e)
        {
            warning ("Failed to load data from resource '%s': %s",
                resource_path, e.message);
            return null;
        }
    }


    /*************************************************************************/
    // URI, File or Path utilities
    // TODO: replace synchronous I/O by asynchronous. Can be hard to adapt all the
    // depending code. File I/O should be fully handled by Tepl, ideally.

    public void delete_file (File file)
    {
        if (!file.query_exists ())
            return;

        try
        {
            file.delete ();
        }
        catch (Error e)
        {
            warning ("Delete file '%s' failed: %s", file.get_parse_name (), e.message);
        }
    }

    public bool save_file (File file, string contents, bool make_backup = false)
    {
        try
        {
            Tepl.utils_create_parent_directories (file, null);

            file.replace_contents (contents.data, null, make_backup,
                FileCreateFlags.NONE, null);
        }
        catch (Error e)
        {
            warning ("Failed to save the file '%s': %s", file.get_parse_name (),
                e.message);
            return false;
        }

        return true;
    }

    // Returns null on error.
    public string? load_file (File file)
    {
        try
        {
            uint8[] chars;
            file.load_contents (null, out chars, null);
            return (string) (owned) chars;
        }
        catch (Error e)
        {
            warning ("Failed to load the file '%s': %s", file.get_parse_name (),
                e.message);
            return null;
        }
    }


    /*************************************************************************/
    // UI stuff

    public ScrolledWindow add_scrollbar (Widget child)
    {
        ScrolledWindow sw = new ScrolledWindow (null, null);
        sw.add (child);
        return sw;
    }

    // get indice of selected row in the treeview
    // returns -1 if no row is selected
    public int get_selected_row (TreeView view, out TreeIter iter = null)
    {
        TreeSelection select = view.get_selection ();
        if (select.get_selected (null, out iter))
        {
            TreeModel model = view.get_model ();
            TreePath path = model.get_path (iter);
            return path.get_indices ()[0];
        }
        return -1;
    }


    /*************************************************************************/
    // Misc

    // FIXME: Horrible hack. Try to get rid of it, and don't use it for new code.
    // It's to turn something asynchronous into a synchronous execution, so can be hard to
    // adapt all the depending code.
    public void flush_queue ()
    {
        while (Gtk.events_pending ())
            Gtk.main_iteration ();
    }
}
