/* SPDX-FileCopyrightText: 2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gtex.h"

static void
test_char_is_escaped (void)
{
	g_assert_false (gtex_utils_char_is_escaped ("a", 0));
	g_assert_true (gtex_utils_char_is_escaped ("\\a", 1));
	g_assert_false (gtex_utils_char_is_escaped ("\\\\a", 2));
	g_assert_true (gtex_utils_char_is_escaped ("\\\\a", 1));
	g_assert_true (gtex_utils_char_is_escaped ("\\\\\\a", 3));
	g_assert_false (gtex_utils_char_is_escaped ("foo\\\\bar", 5));
}

static void
check_relative_path (const gchar *origin_uri,
		     gboolean     origin_is_a_directory,
		     const gchar *target_uri,
		     const gchar *expected_result)
{
	GFile *origin_file;
	GFile *target_file;
	gchar *received_result;

	origin_file = g_file_new_for_uri (origin_uri);
	target_file = g_file_new_for_uri (target_uri);

	received_result = gtex_utils_get_relative_path (origin_file,
							origin_is_a_directory,
							target_file);
	g_assert_cmpstr (received_result, ==, expected_result);
	g_free (received_result);
}

static void
test_get_relative_path (void)
{
	check_relative_path ("file:///home/user/foo", FALSE,
			     "file:///home/user/bar",
			     "bar");
	check_relative_path ("file:///home/user/", TRUE,
			     "file:///home/user/bar",
			     "bar");
	check_relative_path ("file:///home/user/dir/foo", FALSE,
			     "file:///home/user/bar",
			     ".." G_DIR_SEPARATOR_S "bar");
	check_relative_path ("file:///home/user/dir1/dir2/", TRUE,
			     "file:///home/user/bar",
			     ".." G_DIR_SEPARATOR_S ".." G_DIR_SEPARATOR_S "bar");
	check_relative_path ("file:///home/user/dir1/dir2/foo", FALSE,
			     "file:///home/user/dir3/bar",
			     ".." G_DIR_SEPARATOR_S ".." G_DIR_SEPARATOR_S "dir3" G_DIR_SEPARATOR_S "bar");

	/* With remote files */
	check_relative_path ("file:///home/user/foo", FALSE,
			     "https://gedit-text-editor.org/index.html",
			     NULL);

	/* FIXME: doesn't work, issue reported:
	 * https://gitlab.gnome.org/GNOME/gvfs/-/issues/783
	 */
#if 0
	check_relative_path ("https://gedit-text-editor.org/foo", FALSE,
			     "https://gedit-text-editor.org/bar",
			     "bar");
	check_relative_path ("https://gedit-text-editor.org/blog/", TRUE,
			     "https://gedit-text-editor.org/index.html",
			     ".." G_DIR_SEPARATOR_S "index.html");
#endif
}

int
main (int    argc,
      char **argv)
{
	g_test_init (&argc, &argv, NULL);

	g_test_add_func ("/utils/char_is_escaped", test_char_is_escaped);
	g_test_add_func ("/utils/get_relative_path", test_get_relative_path);

	return g_test_run ();
}
